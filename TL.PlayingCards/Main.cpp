// Playing Cards
// Thay Lee

#include <iostream>
#include <conio.h>

using namespace std;

//Rank and suit
enum Rank
{
	Ace = 14,
	King = 13,
	Queen = 12,
	Jack = 11,
	Ten = 10 ,
	Nine = 9,
	Eight = 8,
	Seven = 7,
	Six = 6,
	Five = 5,
	Four = 4,
	Three = 3,
	Two = 2,
};

enum Suit
{
	Spade,
	Heart,
	Diamond,
	Club
};

struct Card
{
	Rank rank;
	Suit suit;
};

int main()
{


	(void)_getch();
	return 0;
}
